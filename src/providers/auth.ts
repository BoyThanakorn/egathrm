import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import { JwtHelper, tokenNotExpired } from 'angular2-jwt';
import 'rxjs/add/operator/map';

/*
  Generated class for the Auth provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AuthService {

  // api_url: string = "http://localhost:8000/api/egatemp/v1/";  
  api_url: string = "https://hrapi.egat.co.th/api/egatemp/v1/";  
  jwtHelper: JwtHelper = new JwtHelper(); 
  authToken : any;
  error: string;	

  constructor(public http: Http, public storage: Storage) {
    this.storage.get('id_token').then(token => {
    	this.authToken = token || null;    
    });
  }

  public authenticated() {
    return tokenNotExpired();
  }

  login(credentials) {
  	let myHeader = new Headers();
  	myHeader.append('Content-Type', 'application/json');
  	return new Promise((resolve, reject) => {
	    this.http.post(this.api_url+'login', JSON.stringify(credentials), { headers: myHeader })
            .map(res => res.json())
            .subscribe(
                data => { 
                    this.authSuccess(data.token);
                    this.storage.set('org', data.org);
                    resolve(data)
                },
                err => {                	
                    this.error = err.json().message;
                    reject(err)
                }
            );
	  });
  }

  logout() {
    this.storage.remove('id_token');
    this.storage.remove('id_token').then(() => { 
      this.authToken = null;
    })
    this.storage.remove('org').then(() => {       
    })
  }

  authSuccess(token) {
    this.error = null;
    this.storage.set('id_token', token);    
    this.authToken = token;    
  }

}
