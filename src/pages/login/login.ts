import { Component } from '@angular/core';
import { MenuController ,NavController, AlertController, LoadingController } from 'ionic-angular';
import { AuthService } from '../../providers/auth';
import { NamePage } from '../name/name';


/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  login: {username?: string, password?: string} = {};
  submitted = false;

  constructor(
    public navCtrl: NavController, 
    public alert: AlertController, 
    public loading: LoadingController,
    public auth: AuthService,
    public menuCtrl: MenuController ) {
    
  }

  ionViewDidLoad() {    
	  console.log('This is Login Page.');	
  }

  onLogin(form) {
    this.submitted = true;

    if (form.valid) {
      let username = this.login.username;
      let password = this.login.password;
      let data = {username,password};

      let loader = this.loading.create({
        content: "กรุณารอสักครู่...",
        dismissOnPageChange: true
      });
      loader.present();

      this.auth.login(data).then(
        (success) => {          
          this.menuCtrl.enable(true);
          this.navCtrl.setRoot(NamePage);
        },
        (err) => {
          loader.dismiss();
          let message = (err.json().message)?err.json().message:'ไม่สามารถเข้าระบบได้';
          let alert = this.alert.create({
            // title: 'Warning',
            subTitle: message,
            buttons: ['OK']
          });
          alert.present();
        }
      );               
    }
  }

}
