import { Component } from '@angular/core';
import { NavController, NavParams,AlertController, LoadingController} from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { LoginPage } from '../login/login'
import { AuthService } from '../../providers/auth';

/*
  Generated class for the Profile page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {

  result: any;
  positions: any;
  dates: any;
  private api_url: string = 'personal/'; 

  constructor(
  	public auth: AuthService,
	  public navCtrl: NavController, 
	  public navParams: NavParams,
	  public http: Http,
    public loading: LoadingController, 
    public alertController: AlertController) {
    console.log('Loading Profile Page');
    let loader = this.loading.create({
      content: "กรุณารอสักครู่...",
    });
    loader.present();
  	let headers = new Headers();
  	headers.append('Authorization','Bearer '+this.auth.authToken);
  	this.result = this.navParams.get('result');
  	this.http.get(this.auth.api_url+this.api_url+this.result.id,{headers:headers}) 
		.subscribe((response) => {      
      this.auth.authSuccess(response.headers.get('Authorization').split(' ')[1]);
			this.positions = response.json().results.positions;
			this.dates = response.json().results.dates;
      loader.dismiss();
      console.log('Creating Profile Page');
		},
    (err)=>{
        loader.dismiss();
        // console.log(err);
        // console.log(err._body.type);
        // console.log(err.type);
        if (err.status < 400) {
          let alert = this.alertController.create({
            title: 'พบข้อผิดพลาด',
            subTitle: 'กรุณาระบุค่าให้ถูกต้อง',
            buttons: ['ตกลง']
        });
        alert.present();  
        }else if(err.status >= 400 &&  err.status < 500){
          let alert = this.alertController.create({
            title: 'พบข้อผิดพลาด',
            subTitle: 'กรุณาเข้าระบบใหม่อีกครั้ง',
            buttons: ['ตกลง']
        });
        alert.present();
          this.auth.logout();
          this.navCtrl.setRoot(LoginPage);            
        }else if(err.status >= 500){
          let alert = this.alertController.create({
            title: 'พบข้อผิดพลาด',
            subTitle: 'แม่ข่ายไม่สามารถทำงานตามที่ร้องขอ',
            buttons: ['ตกลง']
        });
        alert.present();
        }
    });

  }

  ionViewDidLoad() {
    console.log('Hello Profile Page');
  }

}
