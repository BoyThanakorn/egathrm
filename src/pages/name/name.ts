import { Component , ViewChild , ElementRef} from '@angular/core';
import { NavController, NavParams,Platform,ActionSheetController,AlertController ,Content, LoadingController  } from 'ionic-angular';
import { Http, Headers} from '@angular/http';
import { ProfilePage } from '../profile/profile';
import { LoginPage } from '../login/login'
import { AuthService } from '../../providers/auth';
import { Keyboard } from 'ionic-native';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-name',
  templateUrl: 'name.html'
})
export class NamePage {

  results: any;
  keyword: string = '';
  private nextUrl = null;
  private lastPageReached = false;
  private _unfilteredResults: any;
  private usesFilter: boolean = false;
  public ionScroll;
  public showButton = false;
  public contentData = [];
  @ViewChild(Content) content: Content;
  private api_url: string = 'name/'; 
  org : any; 

  constructor(
  	public auth: AuthService,
	public navCtrl: NavController,
	public http: Http,
	public actionSheetCtrl: ActionSheetController,
	public platform: Platform,
	public alertController: AlertController,
	public loading: LoadingController, 
	public navParams: NavParams,
	public myElement: ElementRef,
	public storage: Storage
  ) {
	this.storage.get('org').then(org => {
    	this.org = org || null;    
    });
  }

  ionViewDidLoad() {
    console.log('Hello Name Page');
  }

  userPressedCancle() {
	console.debug('User pressed cancel');
  }

  search(url:string) {
	let loader = this.loading.create({
		content: "กรุณารอสักครู่...",
		dismissOnPageChange: true
	});
	loader.present();  			
	this.usesFilter = false;
	let headers = new Headers();
	headers.append('Authorization','Bearer '+this.auth.authToken);		
	this.http.get(url,{headers:headers})
		.subscribe((response) => {
			loader.dismiss();
			this.auth.authSuccess(response.headers.get('Authorization').split(' ')[1]);
			if(!response.json().data.length) {
				let alert = this.alertController.create({
			      title: 'ผลการค้นหา พบว่า....',
			      subTitle: 'ไม่พบชื่อ นามสุกลดังกล่าว!',
			      buttons: ["ลองอีกครั้ง"]
			    });
			    alert.present();					    
			}else{
				this.results = this.results.concat(response.json().data);
				this._unfilteredResults = this._unfilteredResults.concat(response.json().data);
				this.nextUrl = response.json().next_page_url;
				this.lastPageReached =(this.nextUrl!==null)?false:true;				
				if(this.lastPageReached && this.usesFilter){
					this.results = this._unfilteredResults.filter(
						(item) => item.isBoss === true
					);
				}					
				// this.usesFilter = false;
			}			
		},
	    (err)=>{
	    	loader.dismiss();
	    	// console.log(err);
	    	// console.log(err._body.type);
	    	// console.log(err.type);
	    	if (err.status < 400) {
	    		let alert = this.alertController.create({
				    title: 'พบข้อผิดพลาด',
				    subTitle: 'กรุณาระบุค่าให้ถูกต้อง',
				    buttons: ['ตกลง']
				});
				alert.present();	
	    	}else if(err.status >= 400 &&  err.status < 500){
	    		let alert = this.alertController.create({
				    title: 'พบข้อผิดพลาด',
				    subTitle: 'กรุณาเข้าระบบใหม่อีกครั้ง',
				    buttons: ['ตกลง']
				});
				alert.present();
			    this.auth.logout();
			    this.navCtrl.setRoot(LoginPage);						
	    	}else if(err.status >= 500){
	    		let alert = this.alertController.create({
				    title: 'พบข้อผิดพลาด',
				    subTitle: 'แม่ข่ายไม่สามารถทำงานตามที่ร้องขอ',
				    buttons: ['ตกลง']
				});
				alert.present();
	    	}
	    });
  }

  scrollToTop() {
    this.content.scrollToTop();
  }

  getPersonalData(result:any){
	this.navCtrl.push(ProfilePage, {
	    result: result
	});
  }

  keyHasBeenPressed(e) {
	if(e.keyCode === 13){		
		Keyboard.close();
		this.results = [];
		this._unfilteredResults = [];
		this.showButton = false;		
		if(this.keyword !== ""){
			this.search(this.auth.api_url+this.api_url+this.keyword);
		}else{
			let alert = this.alertController.create({
				title: '',
				subTitle: 'กรุณาระบุคำค้น',
				inputs: [{
					name: 'term',
					placeholder: 'ชื่อ นามสกุล'
		        }],
		        buttons: [
				  {
				    text: 'ยกเลิก',
				  },
				  {
				    text: 'ค้นหา',
				    handler: data => {
				      if(data.term) {
				        this.keyword = data.term;
				        this.search(this.auth.api_url+this.api_url+this.keyword);
				        // automatically dismiss
				        return true;
				      }
				      // Don't allow to dismiss
				      return false;
				    }
				  }
				]
			});
			alert.present();
		}			
	}
  }

  doInfinite(infiniteScroll) {

	setTimeout(() => {			
		if(this.nextUrl!==null){
			this.search(this.nextUrl);
			this.showButton = true;
		}
		infiniteScroll.complete();
	}, 1500);
  }

  isLastPageReached():boolean {
    return this.lastPageReached;
  }

  openFilters() {
	let actionSheet = this.actionSheetCtrl.create({
		title: 'ตัวกรอง',
		cssClass: 'action-sheets-basic-page',
		buttons: [
			{
				text: 'ทั้งหมด',					
				handler: () => {
					this.results = this._unfilteredResults;
					this.usesFilter = false;
					this.showButton = (this.results.length > 15);
				}
			},
			{
				text: 'ผบ. เท่านั้น',					
				handler: () => {
					this.results = this._unfilteredResults.filter(
							(item) => item.isBoss === true
						);
					this.usesFilter = true;
					this.showButton = (this.results.length > 15);
				}
			},
			{
				text: 'ยกเลิก',
				role: 'cancel',
				style: 'cancel',					
				handler: () => {
				}
			}
		]
	});
	actionSheet.present()
  }

}
