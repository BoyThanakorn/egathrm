import { Component, ViewChild } from '@angular/core';
import { MenuController,Nav, Platform , AlertController} from 'ionic-angular';
import { StatusBar } from 'ionic-native';

import { LoginPage } from '../pages/login/login';
import { NamePage } from '../pages/name/name';
import { IdPage } from '../pages/id/id';
import { OrgPage } from '../pages/org/org';
// import { AccountPage } from '../pages/account/account';

import { AuthService } from '../providers/auth';

export interface PageObj {
  title: string;
  component: any;
  icon: string;
  index?: number;
}

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;  

  rootPage: any = LoginPage;
  searchpages: PageObj[] = [
    { title: 'ชื่อ นามสกุล', component: NamePage, icon: 'book' },
    { title: 'เลขประจำตัว', component: IdPage, index: 1, icon: 'card' },
    { title: 'สังกัดย่อ', component: OrgPage, index: 2, icon: 'git-network' }
  ];
  accountpages: PageObj[] = [
    // { title: 'Account', component: AccountPage, icon: 'person' },
    // { title: 'Logout', component: TabsPage, icon: 'log-out' }
  ];
  confirmAlertEnabled = false;

  constructor(public platform: Platform ,public auth: AuthService,public alertController: AlertController,public menuCtrl: MenuController) {
    this.initializeApp();

    // used for an example of ngFor and navigation

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.      
      StatusBar.styleDefault();
      this.menuCtrl.enable(false); 
      this.platform.registerBackButtonAction(() => {
          let nav = this.nav;
          if(nav.canGoBack()){
            nav.pop();
          }else{
            // this.auth.logout();
            // this.platform.exitApp();
            if (!this.confirmAlertEnabled){
              this.confirmExitApp();
            }            
          }

      },100);
    });
  }

  confirmExitApp() {
    this.confirmAlertEnabled = true;
    let alert = this.alertController.create({
      title: 'ยืนยันการปิดระบบ',
      message: 'คุณต้องการปิดระบบหรือไม่?',
      buttons: [
        {
          text: 'ไม่',
          role: 'cancel',
          handler: () => {
            this.confirmAlertEnabled = false;
            console.log('Cancel clicked');
          }
        },
        {
          text: 'ใช่',
          handler: () => {
            this.auth.logout();
            this.platform.exitApp();
          }
        }
      ]
    });
    alert.present();       
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);    
  }

  goBack() {
    console.log('logout');
    this.auth.logout();
    this.menuCtrl.close();
    this.menuCtrl.enable(false); 
    this.nav.setRoot(this.rootPage);
    if (this.platform.is('ios')) {
      // This will only print when on iOS
      console.log("I'm an iOS device!");      
    }else{
      this.platform.exitApp();   
    }    
  }
  
}
