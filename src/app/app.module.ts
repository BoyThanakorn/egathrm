import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../pages/login/login';
import { NamePage } from '../pages/name/name';
import { IdPage } from '../pages/id/id';
import { OrgPage } from '../pages/org/org';
import { ProfilePage } from '../pages/profile/profile';
import { AccountPage } from '../pages/account/account';

import { AuthService } from '../providers/auth';


@NgModule({
  declarations: [
    MyApp,
    NamePage,
    IdPage,
    OrgPage,
    ProfilePage,
    AccountPage,
    LoginPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    NamePage,
    IdPage,
    OrgPage,
    ProfilePage,
    AccountPage,
    LoginPage
  ],
  providers: [ Storage, AuthService  ]
})
export class AppModule {}
